To compile on Linux PC run build_linux.sh (or just build the CMake external-project)

To compile for android it currently does not work to build the external-project
as I have not figured out how to pass multiple search directories to
ECM_ADDITIONAL_FIND_ROOT_PATH or how to force the Qt root directory in the
external-project. That is why I now have a plain script (build_android2.sh) to
just clone the dependencies and build for android. This script also creates the
APK and uploads it to the phone and starts it.

....
