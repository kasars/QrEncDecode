#!/bin/sh

set -e

export ANDROID_SDK_ROOT=/opt/android/adt-bundle-linux-x86_64/sdk/
export ANDROID_NDK=/opt/android/android-ndk-r10e

export QT5_ANDROID=/opt/Qt/5.11.1/android_armv7/

export ANT=/usr/bin/ant
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/

export ANDROID_API_LEVEL="16"
export ANDROID_ARCHITECTURE="arm"


mkdir -p build_android

cd build_android

MAKE_FILE="Makefile"

if [ ! -f $MAKE_FILE ]; then
cmake -DCMAKE_TOOLCHAIN_FILE=/usr/share/ECM/toolchain/Android.cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_INSTALL_PREFIX=./install \
    -DEXTRA_PATHS="$QT5_ANDROID;$(pwd)/install" \
    ../
fi

make -j1

cd EncDeCode-prefix/src/EncDeCode-build

echo "Make install/strip"
make install/strip

echo "create-apk ... "
make create-apk-QrEncDeCode


echo "install APK"
adb install -r QrEncDeCode_build_apk/build/outputs/apk/QrEncDeCode_build_apk-debug.apk

echo "stat APK"
adb shell am start -n org.kde.qrencdecode/org.qtproject.qt5.android.bindings.QtActivity
