#!/bin/sh

set -e

export ANDROID_SDK_ROOT=/opt/android/adt-bundle-linux-x86_64/sdk/
export ANDROID_NDK=/opt/android/android-ndk-r10e

export QT5_ANDROID=/opt/Qt/5.11.1/android_armv7/

export ANT=/usr/bin/ant
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/

export ANDROID_API_LEVEL="16"
export ANDROID_ARCHITECTURE="arm"


mkdir -p bandroid
cd bandroid

mkdir -p install
export INST_DIR=$(pwd)/install

##### QRENCODE #####
mkdir -p libqrencode
cd libqrencode
if [ ! -d "libqrencode" ]; then
    git clone https://github.com/fukuchi/libqrencode.git
fi

mkdir -p build
cd build

cmake -DCMAKE_TOOLCHAIN_FILE=/usr/share/ECM/toolchain/Android.cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_INSTALL_PREFIX=$INST_DIR \
    -DBUILD_SHARED_LIBS=true \
    -DWITH_TOOLS=0 \
    ../libqrencode

make -j6 install

cd ../../



##### LIBZXING #####
mkdir -p libzxing
cd libzxing
if [ ! -d "zxing-cpp" ]; then
    git clone https://github.com/glassechidna/zxing-cpp.git
fi

mkdir -p build
cd build

cmake -DCMAKE_TOOLCHAIN_FILE=/usr/share/ECM/toolchain/Android.cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_INSTALL_PREFIX=$INST_DIR \
    -DBUILD_SHARED_LIBS=true \
    ../zxing-cpp

make -j6 install

cd ../../


##### QrEncDeCode #####
mkdir -p QrEncDeCode
cd QrEncDeCode
mkdir -p build
cd build

cmake -DCMAKE_TOOLCHAIN_FILE=/usr/share/ECM/toolchain/Android.cmake \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_INSTALL_PREFIX=$INST_DIR \
    -DQTANDROID_EXPORTED_TARGET=QrEncDeCode \
    -DANDROID_APK_DIR=../../../data \
    -DECM_ADDITIONAL_FIND_ROOT_PATH:PATH="$QT5_ANDROID;$INST_DIR" \
    ../../../src

make -j6

echo "Make install/strip"
make install/strip

echo "create-apk ... "
make create-apk-QrEncDeCode


echo "install APK"
adb install -r QrEncDeCode_build_apk/build/outputs/apk/QrEncDeCode_build_apk-debug.apk

echo "stat APK"
adb shell am start -n org.kde.qrencdecode/org.qtproject.qt5.android.bindings.QtActivity
