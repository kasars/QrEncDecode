#!/bin/sh

set -e

mkdir -p build_linux
cd build_linux

MAKE_FILE="Makefile"

if [ ! -f $MAKE_FILE ];
then
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=./install ../
fi

make -j6
make -j6 EncDeCode/fast

exec ./install/bin/QrEncDeCode &

