# - Try to find the ZXing library
# Once done this will define
#
#  ZXING_FOUND - system has the zxing library
#  ZXING_INCLUDE_DIR - the zxing library include dir
#  ZXING_LIBRARIES - the libraries used to link zxing

# Copyright (C) 2016 Kåre Särs <kare.sars@iki.fi>

# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (ZXING_INCLUDE_DIR AND ZXING_LIBRARIES)
  # in cache already
  set(ZXING_FOUND TRUE)
else (ZXING_INCLUDE_DIR AND ZXING_LIBRARIES)

  find_path(ZXING_INCLUDE_DIR zxing/ZXing.h)

  find_library(ZXING_LIBRARIES NAMES zxing)

  if (ZXING_INCLUDE_DIR AND ZXING_LIBRARIES)
     set(ZXING_FOUND TRUE)
  else (ZXING_INCLUDE_DIR AND ZXING_LIBRARIES)
     message("ZXing library not found, please see http://.html")
  endif (ZXING_INCLUDE_DIR AND ZXING_LIBRARIES)

  mark_as_advanced(ZXING_INCLUDE_DIR ZXING_LIBRARIES)

endif (ZXING_INCLUDE_DIR AND ZXING_LIBRARIES)
