/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "LuminanceQVectorSource.h"
#include <QDebug>

namespace qrviddec {

LuminanceQVectorSource::LuminanceQVectorSource(int width, int height, const QVector<uchar> &data)
: LuminanceSource(width, height), m_data(data)
{
}

LuminanceQVectorSource::~LuminanceQVectorSource()
{
}

uchar LuminanceQVectorSource::getPixel(int x, int y) const
{
    int index = y*getWidth() + x;
    if (index >= m_data.size() || index < 0) {
        qDebug() << "BAD index" << index << x << y;
        return 0;
    }
    return m_data[index];
}

ArrayRef<char> LuminanceQVectorSource::getRow(int y, ArrayRef<char> row) const
{
    int width = getWidth();

    if (row->size() != width) {
        row.reset(ArrayRef<char>(width));
    }

    for (int x = 0; x<width; ++x) {
        row[x] = getPixel(x,y);
    }

    return row;
}

ArrayRef<char> LuminanceQVectorSource::getMatrix() const
{
    int width = getWidth();
    int height =  getHeight();
    char* matrix = new char[width*height];
    char* m = matrix;

    for(int y=0; y<height; y++) {
        ArrayRef<char> tmpRow;
        tmpRow = getRow(y, ArrayRef<char>(width));
        memcpy(m, tmpRow->values().data(), width);
        m += width * sizeof(unsigned char);
    }

    ArrayRef<char> arr = ArrayRef<char>(matrix, width*height);

    if (matrix) {
        delete matrix;
    }

    return arr;
}


}
