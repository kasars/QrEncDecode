/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include <zxing/LuminanceSource.h>
#include <QVector>

using namespace zxing; 
namespace qrviddec {

class LuminanceQVectorSource : public LuminanceSource {
public:
    explicit LuminanceQVectorSource(int width, int height, const QVector<uchar> &data);
    ~LuminanceQVectorSource();

    ArrayRef<char> getRow(int y, ArrayRef<char> row) const override;
    ArrayRef<char> getMatrix() const override;

private:
    uchar getPixel(int x, int y) const;

    QVector<uchar> m_data;
};

}
