/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "QrCodeFilter.h"
#include "QrDecodeWorker.h"
#include <QDebug>
#include <QByteArray>
#include <QFutureWatcher>
#include <QtConcurrent>

QrCodeFilterRunnable::QrCodeFilterRunnable(QrCodeFilter *filter): m_filter(filter)
{
    m_timer.start();
}

int testResult(const QVector<uchar> &luminanceData, int width, int height, QObject *signalReceiver)
{
    QrDecodeWorker *worker = new QrDecodeWorker();
    QObject::connect(worker, SIGNAL(decodeResult(QString)), signalReceiver, SLOT(slotDecodeResult(QString)));

    worker->decode(luminanceData, width, height);
    return 0;
}

static const QVector<uchar>rgb32ToLuminance(const QVideoFrame &/*mapped*/, int */*width*/, int */*height*/)
{
    QVector<uchar> luminance;
    return luminance;
}

static const QVector<uchar>bgr32ToLuminance(const QVideoFrame &mapped, int *width, int *height)
{
    QVector<uchar> luminance;
    if (!width || !height) {
        return luminance;
    }
    int jump = qMax(mapped.width() / 600, 1);
    jump = qMax(mapped.height() / 500, jump);
    int incr = 4 * jump;

    *width = mapped.width()/jump;
    *height = mapped.height()/jump;

    luminance.resize(*width * *height);

    const uchar *bits = mapped.bits();
    int bpl = mapped.bytesPerLine();

    int i=0;
    for (int y=0; y<*height; ++y) {
        int lStart = y*bpl*jump;
        for (int x=*width-1; x>=0; --x) {
            // NOTICE the frame seems to be flipped horizontally or vertically
            int xStart = x*incr;
            luminance[i] = (uchar)(((int)bits[lStart+xStart] + (int)bits[lStart+xStart+1] + (int)bits[lStart+xStart+2]) / 3);
            i++;
        }
    }

    return luminance;
}

static const QVector<uchar>yuv420ToLuminance(const QVideoFrame &mapped, int *width, int *height)
{
    QVector<uchar> luminance;
    if (!width || !height) {
        return luminance;
    }
    int jump = qMax(mapped.width() / 400, 1);
    jump = qMax(mapped.height() / 300, jump);

    *width = mapped.width()/jump;
    *height = mapped.height()/jump;

    luminance.resize(*width * *height);

    int bpl = mapped.bytesPerLine();
    const uchar *bits = mapped.bits();

    int i=0;
    for (int y=0; y<*height; ++y) {
        int lStart = y*bpl*jump;
        for (int x=0; x<*width; ++x) {
            //qDebug() << i << lStart << y << x << lStart+x << luminance.size() << *height << *width;
            luminance[i] = bits[lStart+x*jump];
            i++;
        }
    }
    return luminance;
}

QVideoFrame QrCodeFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &, RunFlags)
{
    if (m_timer.elapsed() > 300 && m_filter->m_numWorkers < 2) {
        QVideoFrame cloneFrame(*input);
        cloneFrame.map(QAbstractVideoBuffer::ReadOnly);

        QVector<uchar> luminanceData;

        int width = 1;
        int height = 1;


        switch (cloneFrame.pixelFormat())
        {
            case QVideoFrame::Format_ARGB32:
            case QVideoFrame::Format_RGB32:
                qDebug() << "rgb..." << cloneFrame.width() << cloneFrame.bytesPerLine(0) << cloneFrame.fieldType() << cloneFrame.planeCount();
                luminanceData = rgb32ToLuminance(cloneFrame, &width, &height);
                break;
            case QVideoFrame::Format_BGRA32:
            case QVideoFrame::Format_BGR32:
                //qDebug() << "bgr..." << cloneFrame.width() << cloneFrame.bytesPerLine(0) << cloneFrame.fieldType() << cloneFrame.planeCount();
                luminanceData = bgr32ToLuminance(cloneFrame, &width, &height);
                break;
            case QVideoFrame::Format_YUV420P:
                // qDebug() << "YU420P" << cloneFrame.width() << cloneFrame.bytesPerLine(0) << cloneFrame.fieldType() << cloneFrame.planeCount();

                luminanceData = yuv420ToLuminance(cloneFrame, &width, &height);
                break;
            default:
                qDebug() << "Format not supported" << cloneFrame.pixelFormat();
        }

        if (!luminanceData.isEmpty()) {
            // Start the computation.
            m_filter->m_numWorkers++;
            QtConcurrent::run(testResult, luminanceData, width, height, m_filter);
        }
        m_timer.restart();
        cloneFrame.unmap();
    }

    emit m_filter->finished(m_filter);

    return *input;
}

QrCodeFilter::QrCodeFilter(QObject *parent) : QAbstractVideoFilter(parent), m_numWorkers(0)
{
}

void QrCodeFilter::slotDecodeResult(const QString &result)
{
    emit qrCodeFound(result);
    QObject *worker = sender();
    if (worker) {
        m_numWorkers--;
        // NOTE: the sender is created in a different thread and the signal
        // emitting function has already exited.
        delete worker;
    }
}

