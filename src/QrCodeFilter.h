/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#ifndef QrCodeFilter_h
#define QrCodeFilter_h

#include <QAbstractVideoFilter>
#include <QVideoFilterRunnable>
#include <QVideoSurfaceFormat>
#include <QByteArray>
#include <QFutureWatcher>
#include <QElapsedTimer>

class QrCodeFilter;

class QrCodeFilterRunnable : public QVideoFilterRunnable
{
public:
    QrCodeFilterRunnable(QrCodeFilter *filter);
    QVideoFrame	run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags) override;
private:

    QrCodeFilter *m_filter;
    QElapsedTimer m_timer;
};

class QrCodeFilter : public QAbstractVideoFilter
{
    friend class QrCodeFilterRunnable;
    Q_OBJECT
public:
    QrCodeFilter(QObject *parent=0);
    QVideoFilterRunnable *createFilterRunnable() { return new QrCodeFilterRunnable(this); }

private Q_SLOTS:
    void slotDecodeResult(const QString &result);

Q_SIGNALS:
    void finished(QObject *result);

    void qrCodeFound(const QString &str);

private:
    int m_numWorkers;
};

#endif // QrCodeFilter_H
