/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "QrDecodeWorker.h"
#include "LuminanceQVectorSource.h"
#include <QDebug>

#include <zxing/qrcode/QRCodeReader.h>
#include <zxing/Exception.h>
#include <zxing/common/GlobalHistogramBinarizer.h>
#include <zxing/DecodeHints.h>

using namespace zxing;
using namespace zxing::qrcode;
using namespace qrviddec;

void QrDecodeWorker::decode(const QVector<uchar> &data, int width, int height)
{
    try
    {
        //qDebug() << width << height << data.count();
        Ref<LuminanceSource> source (new LuminanceQVectorSource(width, height, data));
        //qDebug() << data.count() << width << height;

        // Turn it into a binary image.
        Ref<Binarizer> binarizer (new GlobalHistogramBinarizer(source));
        Ref<BinaryBitmap> image(new BinaryBitmap(binarizer));

        // Tell the decoder to try as hard as possible.
        DecodeHints hints(DecodeHints::DEFAULT_HINT);
        hints.setTryHarder(true);

        // Perform the decoding.
        QRCodeReader reader;
        Ref<Result> result(reader.decode(image, hints));

        // Output the result.
        qDebug() << result->getText()->getText().c_str();
        emit decodeResult(QString::fromStdString(result->getText()->getText()));
    }
    catch (zxing::Exception& e)
    {
        //qDebug() << "Error: " << e.what();
        emit decodeResult(QString(e.what()));
    }
}

