/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "QrImageProvider.h"

#include <qrencode.h>
#include <QDebug>


QrImageProvider::QrImageProvider() : QQuickImageProvider(QQuickImageProvider::Pixmap) {}

QPixmap QrImageProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    QRcode *qr = QRcode_encodeString8bit(qPrintable(id), 0, QR_ECLEVEL_H);
    if (!qr) {
        return QPixmap();
    }

    QImage img = QImage(qr->width+2, qr->width+2, QImage::Format_RGB32);
    img.fill(QColor("white"));
    for (int i = 0; i < qr->width; i++) {
        for (int j = 0; j < qr->width; j++) {
            if (qr->data[(i * qr->width) + j] & 0x1) {
                img.setPixel(j+1,i+1, 0);
            }
        }
    }
    QRcode_free(qr);

    img = img.scaled(img.width() * 4, img.height() * 4);

    if (size) {
        *size = img.size();
    }
    QPixmap pixmap(requestedSize.width() > 0 ? requestedSize.width() : img.width(),
                   requestedSize.height() > 0 ? requestedSize.height() : img.height());
    pixmap.fill(QColor("white").rgba());

    QPainter painter(&pixmap);
    painter.drawImage(pixmap.rect(), img, img.rect());

    return pixmap;
}
