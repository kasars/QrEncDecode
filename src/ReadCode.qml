/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

import QtQuick 2.3
import QtMultimedia 5.5
import qr.code.helpers 1.0

Item {

    Camera {
        id: camera
        captureMode: Camera.CaptureStillImage
        position: Camera.BackFace

        digitalZoom: pinchArea.zoomScale

        focus {
            focusMode: Camera.FocusMacro + Camera.FocusContinuous
            focusPointMode: Camera.FocusPointCustom
            customFocusPoint: Qt.point(0.5, 0.5)
        }

        flash.mode: Camera.FlashOff
    }

    QrCodeFilter {
        id: filter
        // set properties, they can also be animated
        onQrCodeFound: {
            console.log(str)
            label.text = str
        }
    }

    VideoOutput {
        id: viewfinder
        anchors.fill: parent
        anchors.bottomMargin: label.height
        source: camera
        autoOrientation: true
        filters: [ filter ]

        Repeater {
            model: camera.focus.focusZones

            Rectangle {
                border {
                    width: 2
                    color: status == Camera.FocusAreaFocused ? "green" : "white"
                }
                color: "transparent"

                property variant mappedRect: viewfinder.mapNormalizedRectToItem(area);

                x: mappedRect.x
                y: mappedRect.y
                width: mappedRect.width
                height: mappedRect.height
            }
        }

        PinchArea {
            id: pinchArea
            anchors.fill: parent

            pinch.minimumScale: 1.0
            pinch.maximumScale: 10.0

            property double zoomScale: 1.0

            onPinchUpdated: {
                var newZoom =  zoomScale + pinch.scale - pinch.previousScale;
                if (newZoom > 10) newZoom = 10.0;
                if (newZoom < 1) newZoom = 1.0;
                zoomScale = newZoom;
            }

            MouseArea {
                id: mArea
                anchors.fill: parent
                onClicked: {
                    camera.focus.customFocusPoint = viewfinder.mapPointToSourceNormalized(Qt.point(mouse.x, mouse.y))
                    camera.searchAndLock()
                }
            }
        }

        Image {
            anchors {
                top: parent.top
                right: parent.right
                margins: 10
            }
            width: Math.min(parent.width *0.2, parent.height*0.2)
            height: width
            source: camera.flash.mode === Camera.FlashVideoLight ? "Images/Flash.png" : "Images/NoFlash.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    camera.flash.mode = camera.flash.mode === Camera.FlashVideoLight ?  Camera.FlashOff : Camera.FlashVideoLight
                }
            }
        }

    }

    Text {
        id: label
        anchors {
            left: parent.left
            right:parent.right
            bottom:parent.bottom
        }
        text: "Result here"
    }


}
