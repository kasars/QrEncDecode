/*
 *  Copyright (C) 2018 Kåre Särs <kare.sars@iki.fi>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

import QtQuick 2.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtQuick.Controls 1.4

Window {
    visible: true
    height:600
    width:400

    TabView {
        id: tabView
        anchors.fill: parent
        focus: true
        frameVisible: true
        tabsVisible: true
        Tab {
            title: qsTr("Encode")
            QrEncode { }
        }
        Tab {
            title: qsTr("Decode")
            ReadCode {}
        }
    }
}
